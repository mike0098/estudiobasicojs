//FUNCION EXPRESSION

const sumar = function(){
    console.log(2 * 7)
}

//ARROW FUNCTION
const sumar0 = () =>{  
    console.log(2 * 5)
}

const sumar1 = () => 2 * 2
const sumar2 = numero => numero * 2
const sumar3 = (numero1, numero2) => numero1 * numero2

sumar()
sumar0()
let resultado1 = sumar1()
let resultado2 = sumar2(3)
let resultado3 = sumar3(3,4)

console.log(resultado1)
console.log(resultado2)
console.log(resultado3)