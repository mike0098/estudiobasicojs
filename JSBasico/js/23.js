//async await

const url = "https://jsonplaceholder.typicode.com/comments";

const url2 = "https://jsonplaceholder.typicode.com/photos";

const consultComment = async () =>  {
    //el await preveine que se continue el codigo hasta que se termine el llamado
    let inicio = performance.now()
    const respuesta = await fetch(url);
    const resultado = await respuesta.json();

    const respuesta2 = await fetch(url2);
    const resultado2 = await respuesta2.json();

    let fin = performance.now()
    console.log(`primero ${fin -inicio}`)
};
consultComment()


const consultComment2 = async () =>  {
    const inicio1 = performance.now()
    const [res1, res2] = await Promise.all([fetch(url),fetch(url2)]);
    const resul1 = await res1.json();
    const resul2 = await res2.json();
    
    const fin1 = performance.now()
    console.log(`segundo ${fin1 - inicio1}`)
}

consultComment2()

 


// consultComment().then(res => {
//     res.forEach(x =>{
//         console.log(x)
//     })
// })
