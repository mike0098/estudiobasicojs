//async await

const url = "https://jsonplaceholder.typicode.com/comments";

const consultComment = async () =>  {
    //el await preveine que se continue el codigo hasta que se termine el llamado
    const respuesta = await fetch(url);
    const resultado = await respuesta.json();
    return resultado;
};


consultComment().then(res => {
    res.forEach(x =>{
        console.log(x)
    })
})
