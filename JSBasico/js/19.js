//eventos submit

const formulario = document.querySelector('#formulario');

formulario.addEventListener('submit', e => {
    e.preventDefault();

    const nombre = document.querySelector('.nombre').value;
    const passw = document.querySelector('.password').value;

    const alertaPrevia = document.querySelector('.alerta');

    if(alertaPrevia){
        alertaPrevia.remove();
    }

    const alerta = document.createElement('DIV');
    alerta.classList.add('alerta');

    
    
    if(nombre === "" || passw === ""){
        alerta.classList.add('error');
        alerta.textContent = "todos los campos son obligatorios";
    }else{
        alerta.classList.add('exito');s
        alerta.textContent = "enviando";
    }

    formulario.appendChild(alerta);
    console.log(nombre);
    console.log(passw);
    console.log('enviaste un formulario')
})