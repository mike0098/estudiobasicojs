const producto = {
    nombre: "tablet",
    precio: 400000,
    disponible: true,
}

const cliente = {
    nombre: "juan",
    premium: true
}


const { nombre, precio, disponible} = producto;
//nombrecliente  es un alias que se le crea a la propiedad del objeto para que no se dupliquen las 
//variables.
const { nombre: nombreCliente, premium} = cliente;