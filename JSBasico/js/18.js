//evento click

heading = document.querySelector(".heading");

//heading.addEventListener('click', 
//  e => {
//      console.log('presionaste el heading')
//});

heading.addEventListener('click', 
    e => {
        console.log('presionaste el heading')
});

//addEventListener no se puede ejecutar para un grupo de elementos HTML

//evento Input 

let nombre = document.querySelector('.nombre');

nombre.addEventListener('input', () =>
    console.log("escribiendo en el input")
);
            
nombre.addEventListener('keydown', () =>
    console.log("presionaste una tecla en el input")
);

nombre.addEventListener('keyup', () =>
    console.log("soltaste una tecla en el input")
);

nombre.addEventListener('keyup', e =>
    console.log(e.target.value)
);

let passw =  document.querySelector('.password')

passw.addEventListener('keyup', (e) => {
    passw.type = 'text';
    setTimeout(() =>{
        passw.type = 'password'
    }, 1000);
});
