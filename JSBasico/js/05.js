//objetos manipulacion

const producto = {
    nombre: "tablet",
    precio: 400000,
    disponible: true,
}

//se puede modificar una propiedad a pesar de ser una variable const
producto.nombre = "pantalla"

//se puede añadir una nueva propiedad solo poniendo la propiedad en el producto 
producto.imagen = "imagen.jpg"

//eliminar propiedad
delete producto.imagen;

//como hacer para que no se modifique nada del objeto, añadir, eliminar, o modificar la propiedades 
Object.freeze(producto);
//permite solo modificar la propiedad
Object.seal(producto);
