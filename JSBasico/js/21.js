//Fetch API

const url = "https://jsonplaceholder.typicode.com/comments";


//promesas
let consultComment = () =>  {
    fetch(url)
        .then( respuesta => respuesta.json())
        .then( resultado => {
            resultado.forEach(comentario =>
                console.log(comentario)
            )
        })
};

consultComment();

