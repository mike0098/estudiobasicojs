//unir dos objetos o mas en un mismo objeto 


const producto = {
    nombre: "tablet",
    precio: 400000,
    disponible: true,
}

const cliente = {
    nombre: "juan",
    premium: true
}

const nuevoObjeto2 = {
    producto: {...producto},
    cliente: {...cliente}
}