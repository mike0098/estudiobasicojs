//array destruction
// function sumar(numero =0 , numero2 =9){
//     return [numero * numero2, "hola mundo!"]
// }

// const [resultado, holamundo] = sumar(3,2);

//object  destruction
function sumar(numero =0 , numero2 =9){
    return {
        resultado: numero * numero2,
        mensaje: "hola mundo!"
    }
}

const {resultado, mensaje} = sumar(3,2);
