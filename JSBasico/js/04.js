let producto = {
    nombre: "tablet",
    precio: "400000",
    disponible: "true",
}

//crea las variables locales asignando las propiedades del objeto a las variables con el mismo nombre 
//destructuring
const { nombre, precio } = producto;


//object literal enhacement
const autenticado = true;
const usuario = juan ;

const nuevoobjeto = {
    autenticado: autenticado,
    usuario: usuario,
}

//se puede crear un objeto asignando como propiedades las variables ya creadas, las propiedades 
//quedaran con el nombre de la variable creada
const nuevoobjeto2 = {
    autenticado,
    usuario,
}