
const tecnologias = ['HTML','CSS','Javascript','React','Node.js','graphql']

const numero = [25,25,25]


//true - false si existe el elemento en el arreglo
const result = tecnologias.includes('graphql');
//true - false devuelve si al alguno cumple la condicion
const resultado = numero.some(x => x > 20)
//desvuelve el primero que cumpla la condicion
const resultado1 = numero.find(x => x > 20)
//true - false devuelve si todos cumplen la condicion
const resultado2 = numero.every(x => x > 20)
//reduce
const resultado3 = numero.reduce((total, actual) => total + actual, 0)
//forEach
const resultado4 = numero.forEach((tech, index) => index)

console.log(resultado3);


/*

comparacion no estricta compara por valor 
==
comparacion estricta compara por valor y tipo dev dato
===

*/