const tecnologias = ['HTML','CSS','Javascript','React','Node.js']

//agergar al final
tecnologias.push('graphql')

//agregar al inicio
tecnologias.unshift('graphql')

//nuevo arreglo sin modificar el anterior al inicio
//const newarray = [...tecnologias,'graphql']

//nuevo arreglo sin modificar el anterior al final 
//const newarray = [...tecnologias,'graphql']

//eliminar el inicio
//tecnologias.pop()

//eliminar al inicio 
//tecnologias.shift()

//eliminar en una posicion en especifico
//posicion a borrar
//elementos a borrar
tecnologias.splice(1, 1)

// const newarray = tecnologias.filter(function(tech){
//     return  tech !== "graphql"
// });


// const newarray = tecnologias.map(function(tech){
//     if(tech === "html"){
//         return 'graphql'
//     }else{
//         return tech
//     }
// });